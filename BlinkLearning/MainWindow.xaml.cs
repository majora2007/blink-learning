﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EmoEngineClientLibrary;


namespace BlinkLearning
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        EmoEngineClient emoEngineClient;
        System.Windows.Forms.Timer blinkTimer = new System.Windows.Forms.Timer();
        const int blinkIntervalTime = 6; // Number of seconds between blinks

        public MainWindow()
        {
            InitializeComponent();

            this.Activate();
            this.Topmost = true;  // important
            this.Topmost = false; // important
            this.Focus();         // important


            this.emoEngineClient = this.FindResource("emoEngineClient") as EmoEngineClient;
            this.emoEngineClient.ActivePort = EmoEngineClient.ControlPanelPort;

            emoEngineClient.PropertyChanged += emoEngineClient_PropertyChanged;

            blinkTimer.Interval = (1000) * (blinkIntervalTime);
            blinkTimer.Tick += this.blinkTimer_OnTick;
            
            emoEngineClient.StartEmoEngine();

        }

        private void blinkTimer_OnTick(object sender, EventArgs e)
        {
            this.Show();
        }

        void BlinkWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Console.WriteLine("You are not allowed to close window!");
            e.Cancel = true;
        }

        void emoEngineClient_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentEmotivState")
            {
                EmotivState emoState = emoEngineClient.CurrentEmotivState;

                if (emoState.ExpressivLeftEyelidState == 1 && emoState.ExpressivRightEyelidState == 1)
                {
                    // User blinked!
                    HandleBlink();
                }
            }
        }

        private void HandleBlink()
        {
            // if already hidden, just restart timer
            // else hide window
            if (this.IsVisible)
            {
                HideWindow();
            }
            else
            {
                this.blinkTimer.Stop();
                this.blinkTimer.Start();
            }
        }

        private void HideWindow()
        {
            this.Hide();
            this.blinkTimer.Start();
        }

        private void OnDeactivated(object sender, EventArgs e)
        {
            /*Console.WriteLine("Decativated: " + e.GetType());

            if (blinkTimer.Enabled)
            {
                this.Activate();
                this.Topmost = true;  // important
                this.Focus();         // important
            }*/
            
        }

    }
}
