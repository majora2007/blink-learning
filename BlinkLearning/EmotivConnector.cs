﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emotiv;

namespace BlinkLearning
{
    class EmotivConnector
    {
        private static EmotivConnector instance;

        protected EmoEngine engine; // Access to the EDK is viaa the EmoEngine 
        private uint userID; // userID is used to uniquely identify a user's headset

        private float elapsedTime = 0;

        private EmotivConnector() { }

        public static EmotivConnector Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EmotivConnector();
                }

                return instance;
            }
        }

        // Update is called once per frame
        public void Update()
        {
            if (engine == null) return;

            // Handle any waiting events
            engine.ProcessEvents();
        }

        public uint getActiveUser()
        {
            return (userID);
        }

        public static EmoEngine getEmoEngine()
        {
            if (instance.engine == null) return EmoEngine.Instance;
            else return instance.engine;
        }

        public void connect()
        {
            engine = EmoEngine.Instance;

            engine.EmoEngineConnected += new EmoEngine.EmoEngineConnectedEventHandler(engine_EmoEngineConnected);
            engine.EmoStateUpdated += new EmoEngine.EmoStateUpdatedEventHandler(engine_EmoStateUpdated);

            engine.Connect();

        }

        public void disconnect()
        {
            try
            {
                engine.Disconnect();
            }
            catch { }

            engine = null;
        }

        

        public bool isConnected()
        {
            return (engine != null && engine.EngineGetNumUser() > 0);
        }


        void engine_EmoEngineConnected(object sender, EmoEngineEventArgs e)
        {

            Console.WriteLine("EmoEngine Connected!");
            userID = e.userId;
           
            Console.WriteLine("User ID: " + userID);
        }


        void engine_EmoStateUpdated(object sender, EmoStateUpdatedEventArgs args) 
        {
		
            EmoState emoState = args.emoState; 
            float leftEyelidState, rightEyelidState;
            emoState.ExpressivGetEyelidState(out leftEyelidState, out rightEyelidState);

            Console.WriteLine("LEFT eyelid state is {0} and RIGHT eyelid state is {1}.", leftEyelidState, rightEyelidState);
            
            if (leftEyelidState == 1.0f && rightEyelidState == 1.0f)
            {
                // User has blinked! Trigger event

            }
            
        }

        
    }
}
